using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using System.IO;
using XMLManager;
using ABB.Logger;


using WordManager;

namespace ABB.PLM.Offer
{
    public class OfferLetterGenerator
    {
        #region Declaration of Variables

        Hashtable output;
        WordLoad objwdL = new WordLoad();
        public string xmlfileName;
        public OfferLetterNode root;
        public LoggerClass objLog = null;
   
        #endregion
        #region Declaration of Constants
        public const string TECHNICALDETAILS = "TechnicalDetails";
        public const string SCOPEOFSUPPLY = "ScopeDetails";
        public const string EQUIPMENTDETAILS = "EquipmentDetails";
		public const string EQUIPMENTDETAILS1 = "EquipmentDetails1";

        public const string SUMMARYTABLES = "SummaryTable";
        #endregion

        #region OfferLetterGenerator Constuctor
        public OfferLetterGenerator()
        {

            objLog = LoggerClass.getInstance();
            
            string currentPath = Path.GetDirectoryName(System.Reflection.Assembly.
                 GetExecutingAssembly().GetName().CodeBase);
           string docPath = currentPath + "\\" + "Input\\Project_Offer_Letter.xml";
		   
		   //objLog.WriteLogFile(docPath);
			//string currentPath = "‪C:\\Project_Offer_Letter.xml";
			//string docPath = currentPath;

            try
            {

                XmlDocument objDoc = new XmlDocument();
                objLog.WriteLogFile("Loading xml document");
                objDoc.Load(docPath);
                objLog.WriteLogFile("Loading xml document successful");
                XmlElement objRoot = objDoc.DocumentElement;
                objLog.WriteLogFile("Getting Root element in xml document");
                root = new OfferLetterNode(objRoot);
            }

            catch (Exception docLoadEx)
            {
                objLog.WriteLogFile(docLoadEx.Message.ToString() );
            }


        }
        #endregion

        #region Get BookMarks
        public bool generateOfferLetter()
        {
            try
            {

                List<OfferLetterNode> bookMarks = root.getChildren();
                String searchText = "";


                for (int i = 0; i < bookMarks.Count; i++)
                {
                    searchText = bookMarks[i].getPropertyValue("SearchText");
                    objLog.WriteLogFile("BookMark:"+searchText );
                    if (searchText != null && searchText.Length > 1)
                    {
                        WriteBookMarkTable(bookMarks[i]);

                    }

                }

            }
            catch (Exception bookEX)
            {
                objLog.WriteLogFile(bookEX.Message.ToString());
            }
            finally
            {
                objwdL.UnLoad();
            }

            return true;
        }
        #endregion

        #region Write BookMark Details
        public void WriteBookMarkTable(OfferLetterNode bookMarkNode)
        {
            try
            {
                bool docStat = objwdL.isWordAppLoaded();
                bool bookbool;
                if (docStat == false)
                {
                    objwdL.Load();
                }

                objLog.WriteLogFile("Word Document Loaded Successfully");
                bookbool = objwdL.ISBookMarkAvailable(bookMarkNode);

                if (bookbool == false)
                {
                        objLog.WriteLogFile("BookMark is not available");
                        return;
                }
                else
                {
                    switch (bookMarkNode.getName())
                    {


                       /* case TECHNICALDETAILS:
                         objwdL.CreateTechnicalDetailsTable(bookMarkNode);
                            break;

                        case SCOPEOFSUPPLY:
                        objwdL.CreateScopeDetailsTable(bookMarkNode);
                            break;

                        case EQUIPMENTDETAILS:
                            Console.WriteLine("EQUIPMENTDETAILS");
                      objwdL.CreateEquipmentDetailsTable(bookMarkNode);
                            break;

                        case EQUIPMENTDETAILS1:
                            Console.WriteLine("EQUIPMENTDETAILS1");
                            objwdL.CreateEquipmentDetailsTable1(bookMarkNode);
                            break;
                            */
                        case SUMMARYTABLES:
                            Console.WriteLine("SUMMARYTABLES");
                            objwdL.CreateSummaryTable(bookMarkNode);
                            break;

                        default:
                            break;
                    }




                }
            }
            catch (Exception writeEX)
            {
                objLog.WriteLogFile(writeEX.Message.ToString());
            }


        }
        #endregion

    }
}