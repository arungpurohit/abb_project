using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using ABB.Logger;
namespace WordManager
{
    class ScopeDetailsClass
    {
        #region Declaration of Variables
        OfferLetterNode scopeDetailsNode = null;
        Word._Document iDoc;
        Word.Table iTable;
        Word.Range iRange;
        Word.Paragraph iPara;
        public LoggerClass objLog = null;
        object oMissing = System.Reflection.Missing.Value;

        int switchGearCount;

        #endregion

        #region Constructor
        public ScopeDetailsClass(OfferLetterNode bookMarkNode, Word._Document iDoc)
        {
            objLog = LoggerClass.getInstance();
            this.scopeDetailsNode = bookMarkNode;
            this.iDoc = iDoc;

            writeContents();

        }
        #endregion

        public void writeContents()
        {
            try
            {
                String strSearchText = scopeDetailsNode.getPropertyValue("SearchText");

              //  Console.WriteLine("strSearchText");
              //  Console.WriteLine(strSearchText);

                object bMark = (object)strSearchText;
                iRange = iDoc.Bookmarks.get_Item(ref bMark).Range;
                object oRang = iRange;

                iPara = iDoc.Content.Paragraphs.Add(ref oRang);

                List<OfferLetterNode> liSwgear = scopeDetailsNode.getChildrenOfType("Switchgear");

                switchGearCount = liSwgear.Count;
               // Console.WriteLine("SWITCH GEAR COUNT(writeContents)");
              //  Console.WriteLine(switchGearCount);

                for (int i = liSwgear.Count; i > 0; i--)
                {
                    OfferLetterNode swGearNode = liSwgear[i - 1];

                    writeSwGearContents(swGearNode);
                    iPara = iDoc.Content.Paragraphs.Add(ref oRang);

                    object orng = iPara.Range;
                    Word.Paragraph iParatemp = iDoc.Paragraphs.Add(ref orng);
                    iParatemp.Range.Text = "\r";
                    object ct = 1;
                    iPara = iPara.Previous(ref ct);

                    switchGearCount--;
                }
                //iPara.LeftIndent = 150;

                //iPara.Range.Font.Bold = 1;
                //iPara.Range.Font.Name = "Arial";
                //iPara.Range.Text = "Supply Details of Individual LV Switchgear";


                //iPara.Range.Font.Underline = 0;
            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString());
            }

        }

        private void writeSwGearContents(OfferLetterNode swGearNode)
        {
            try
            {

                String strSwGearName = swGearNode.getPropertyValue("Name");
              //  Console.WriteLine("writeSwGearContents ");
               // Console.WriteLine(strSwGearName);

                iPara.Range.Font.Name = "Arial";
                iPara.LeftIndent = 150;
                iPara.Range.Font.Bold = 1;
                iPara.Range.Text = "\r\r\r" + switchGearCount + ": " + strSwGearName + "\r\r";


                Word.Range inRange = iPara.Range;
                writeHeadingTable(swGearNode, inRange);

                writeCubicleContents(swGearNode);

            }
            catch (Exception objswEX)
            {
                throw;
            }


        }


        private void writeCubicleContents(OfferLetterNode swGearNode)
        {
            try
            {
                iPara.LeftIndent = 150;
                iPara.Range.Font.Bold = 1;
                iPara.Range.Font.Italic = 1;
                iPara.Range.Font.Name = "Arial";
                iPara.Range.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
                iPara.Range.Text = "\r\rCUBICLE/MODULE DETAILS\r\r\r";

                Word.Range inRange = iPara.Range;

                writeCubicleTable(swGearNode, inRange);
            }
            catch (Exception objCuEx)
            {
                throw;
            }


        }


        private void writeCubicleTable(OfferLetterNode swCubsNode, Word.Range inRange)
        {
            try
            {

                int finalCount = 0;

                List<OfferLetterNode> liSwCubs = swCubsNode.getChildrenOfType("Cubicle");

                //getcubiclemoduledetailsTableTotalRowCount
                int cubCount = liSwCubs.Count;
                Console.WriteLine("getcubiclemoduledetailsTableTotalRowCount ");
                Console.WriteLine(cubCount);

                finalCount = finalCount + (cubCount * 2) + 1;


                for (int j = 0; j < cubCount; j++)
                {
                    List<OfferLetterNode> liModules = liSwCubs[j].getChildrenOfType("Module");
                    int modCount = liModules.Count;
                    finalCount = finalCount + modCount;
                }

                Console.WriteLine("Module count ");
                Console.WriteLine(finalCount);

                creatTable(finalCount, 9, inRange);

                iTable.Cell(1, 1).Range.Text = "Cubicle Type";
                iTable.Rows[1].Cells.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray25;

                iTable.Rows[1].Range.Font.Name = "Arial";
                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;

                iTable.Cell(1, 2).Range.Text = "Configured Module�s List";

                iTable.Cell(1, 3).Range.Text = "Quantity";

                iTable.Cell(1, 4).Range.Text = "A";

                iTable.Cell(1, 5).Range.Text = "B";

                iTable.Cell(1, 6).Range.Text = "C";

                iTable.Cell(1, 7).Range.Text = "D";
                iTable.Cell(1, 8).Range.Text = "E";

                iTable.Cell(1, 9).Range.Text = "F";


                iTable.Rows[1].Range.Font.Name = "Arial";

                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;
                iTable.Cell(1, 1).WordWrap = false;



                int rowcount = 2;

                for (int j = 0; j < cubCount; j++)
                {
                    iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);


                    OfferLetterNode cubicleNode = liSwCubs[j];

                    String strCubName = cubicleNode.getPropertyValue("Name");
                    String strCubQtyValue = cubicleNode.getPropertyValue("Quantity");

                    String aValue = cubicleNode.getPropertyValue("A");

                    String bValue = cubicleNode.getPropertyValue("B");
                    String cValue = cubicleNode.getPropertyValue("C");

                    String dValue = cubicleNode.getPropertyValue("D");
                    String eValue = cubicleNode.getPropertyValue("E");

                    String fValue = cubicleNode.getPropertyValue("F");

                    iTable.Cell(rowcount, 1).Range.Text = strCubName;

                    iTable.Rows[rowcount].Range.Font.Name = "Arial";
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;

                    iTable.Cell(rowcount, 3).Range.Text = strCubQtyValue;
                    iTable.Rows[rowcount].Range.Font.Name = "Arial";

                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;


                    iTable.Cell(rowcount, 4).Range.Text = aValue;
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;

                    iTable.Cell(rowcount, 5).Range.Text = bValue;
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;

                    iTable.Cell(rowcount, 6).Range.Text = cValue;
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;

                    iTable.Cell(rowcount, 7).Range.Text = dValue;
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;

                    iTable.Cell(rowcount, 8).Range.Text = eValue;
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;

                    iTable.Cell(rowcount, 9).Range.Text = fValue;
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;
                    iTable.Cell(rowcount, 1).WordWrap = false;

                    iTable.Rows[rowcount].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                    rowcount++;
                    List<OfferLetterNode> liModules = cubicleNode.getChildrenOfType("Module");

                    for (int k = 0; k < liModules.Count; k++)
                    {


                        OfferLetterNode modNode = liModules[k];
                        String strModName = modNode.getPropertyValue("Name");
                        String strModQtyValue = modNode.getPropertyValue("Quantity");



                        String straValue = modNode.getPropertyValue("A");

                        String strbValue = modNode.getPropertyValue("B");


                        String strcValue = modNode.getPropertyValue("C");

                        String strdValue = modNode.getPropertyValue("D");


                        String streValue = modNode.getPropertyValue("E");

                        String strfValue = modNode.getPropertyValue("F");


                        iTable.Cell(rowcount, 2).Range.Text = strModName;


                        // iTable.Cell(rowcount, 4).Range.Text = straValue;

                        //  iTable.Cell(rowcount, 5).Range.Text = strbValue;


                        iTable.Rows[rowcount].Range.Font.Name = "Arial";
                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;

                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Cell(rowcount, 3).Range.Text = strModQtyValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;



                        iTable.Cell(rowcount, 4).Range.Text = straValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;

                        iTable.Cell(rowcount, 5).Range.Text = strbValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;

                        iTable.Cell(rowcount, 6).Range.Text = strcValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Cell(rowcount, 7).Range.Text = strdValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Cell(rowcount, 8).Range.Text = streValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Cell(rowcount, 9).Range.Text = strfValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;

                        rowcount++;
                    }

                    rowcount++;
                }
            }
            catch (Exception tableEx)
            {
                objLog.WriteLogFile(tableEx.Message.ToString());
            }


        }


        private void writeHeadingTable(OfferLetterNode swGearNode, Word.Range inRange)
        {

            try
            {
                int finalCount = 0;

                List<OfferLetterNode> liHeading = swGearNode.getChildrenOfType("Heading");

                //getTechnicalDetailsTableTotalRowCount
                int HeadingCount = liHeading.Count;

                finalCount = finalCount + HeadingCount;


                for (int j = 0; j < HeadingCount; j++)
                {
                    List<OfferLetterNode> liRows = liHeading[j].getChildrenOfType("Rows");
                    int rowCount = liRows.Count;
                    finalCount = finalCount + rowCount;
                }

                creatTable(finalCount, 3, inRange);

                int rowcount = 1;

                for (int j = 0; j < HeadingCount; j++)
                {
                    iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);


                    OfferLetterNode headingNode = liHeading[j];

                    String strheadingName = headingNode.getPropertyValue("Name");
                    iTable.Cell(rowcount, 1).Range.Text = strheadingName;
                    iTable.Rows[rowcount].Cells.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray25;

                    iTable.Rows[rowcount].Range.Font.Name = "Arial";
                    iTable.Rows[rowcount].Range.Font.Bold = 1;
                    iTable.Rows[rowcount].Range.Font.Italic = 0;
                    iTable.Rows[rowcount].Range.Font.Underline = 0;

                    iTable.Rows[rowcount].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                    rowcount++;
                    List<OfferLetterNode> liRows = headingNode.getChildrenOfType("Rows");

                    for (int k = 0; k < liRows.Count; k++)
                    {


                        OfferLetterNode rowNode = liRows[k];
                        String strRowName = rowNode.getPropertyValue("Name");
                        String strValue = rowNode.getPropertyValue("Value");


                        iTable.Cell(rowcount, 2).Range.Text = strRowName;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";
                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;

                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Cell(rowcount, 3).Range.Text = strValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Rows[rowcount].Range.Font.Italic = 0;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;

                        rowcount++;
                    }
                }
                iTable.Rows.Height = 12f;
                iTable.AutoFitBehavior(Microsoft.Office.Interop.Word.WdAutoFitBehavior.wdAutoFitWindow);


            }
            catch (Exception objExe)
            {
                objLog.WriteLogFile(objExe.Message.ToString());
            }
        }

        public void creatTable(int rows, int columns, Word.Range inRange)
        {
            try
            {
                iTable = iDoc.Tables.Add(inRange, rows, columns, ref oMissing, ref oMissing);
                for (int j = 1; j < columns; j++)
                {
                    // if (j == 1 || j == 3)
                    //   iTable.Columns[j].Width = 0.0f;

                }
                iTable.AllowPageBreaks = true;
                iTable.Borders.Enable = 1;
                iTable.Range.ParagraphFormat.SpaceAfter = 6;
                iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);



            }
            catch (Exception tablecrEX)
            {
                objLog.WriteLogFile("Scope Details Table creation Error");
            }
        }

    }
}
