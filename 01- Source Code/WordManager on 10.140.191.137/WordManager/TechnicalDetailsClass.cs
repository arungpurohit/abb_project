using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using ABB.Logger;

namespace WordManager
{
    class TechnicalDetailsClass
    {
        #region Declaration of Variables
        OfferLetterNode technicalNode = null;
        Word._Document iDoc;
        Word.Table iTable;
        Word.Range iRange;
        object oMissing = System.Reflection.Missing.Value;
        public LoggerClass objLog = null;

       
        #endregion

        #region Constructor
        public TechnicalDetailsClass(OfferLetterNode bookMarkNode, Word._Document iDoc)
        {
            objLog = LoggerClass.getInstance();
            this.technicalNode = bookMarkNode;
            this.iDoc = iDoc;
            writeContents();
            iTable.Rows.SetHeight(12.0f, Word.WdRowHeightRule.wdRowHeightAtLeast);
            iTable.AutoFitBehavior(Microsoft.Office.Interop.Word.WdAutoFitBehavior.wdAutoFitWindow);
         
        }
        #endregion


        #region Write Data
        public void writeContents()
        {
            try
            {
                String strSearchText = technicalNode.getPropertyValue("SearchText");

                object bMark = (object)strSearchText;
                iRange = iDoc.Bookmarks.get_Item(ref bMark).Range;
                int finalCount = 0;

                List<OfferLetterNode> liHeading = technicalNode.getChildrenOfType("Heading");

                int HeadingCount = liHeading.Count;

                finalCount = finalCount + HeadingCount;

                for (int j = 0; j < HeadingCount; j++)
                {
                    List<OfferLetterNode> liRows = liHeading[j].getChildrenOfType("Rows");

                    int rowCount = liRows.Count;
                    finalCount = finalCount + rowCount;
                }

                creatTable(finalCount, 3);

                int rowcount = 1;

                for (int j = 0; j < HeadingCount; j++)
                {
                    iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);

                    OfferLetterNode headingNode = liHeading[j];

                    String strheadingName = headingNode.getPropertyValue("Name");

                    iTable.Cell(rowcount, 1).Range.Text = strheadingName;
                    iTable.Rows[rowcount].Cells.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray25;

                    iTable.Rows[rowcount].Range.Font.Name = "Arial";
                    iTable.Rows[rowcount].Range.Font.Bold = 1;

                    iTable.Rows[rowcount].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                    rowcount++;
                    List<OfferLetterNode> liRows = headingNode.getChildrenOfType("Rows");

                    for (int k = 0; k < liRows.Count; k++)
                    {


                        OfferLetterNode rowNode = liRows[k];
                        String strRowName = rowNode.getPropertyValue("Name");

                        String strValue = rowNode.getPropertyValue("Value");

                        iTable.Cell(rowcount, 2).Range.Text = strRowName;
                        iTable.Cell(rowcount, 2).WordWrap = true ;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";
                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Cell(rowcount, 3).Range.Text = strValue;
                        iTable.Cell(rowcount, 3).WordWrap = true;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";
                        iTable.Rows[rowcount].Range.Font.Bold = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;
                    
                        rowcount++;

                    }
                }
               
              
            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString());
            }

        }
        #endregion

        public void creatTable(int rows, int columns)
        {
            try
            {
                if (iTable == null)
                {
                    iTable = iDoc.Tables.Add(iRange, rows, columns, ref oMissing, ref oMissing);
                    iTable.AllowPageBreaks = true;
                    iTable.Borders.Enable = 1;
                    iTable.Range.ParagraphFormat.SpaceAfter = 6;
                    iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);

                    for (int j = 1; j <= columns; j++)
                    {
                       //  if(j==2 || j==4)
                       // {
                           //   iTable.Columns[j].Width =0f;
                      //  }
                        // iTable.Columns[j].Width = 0.0f;
                        

                    }
                }
            }
            catch (Exception exObj)
            {
                throw;
            }
        }

    }

}
