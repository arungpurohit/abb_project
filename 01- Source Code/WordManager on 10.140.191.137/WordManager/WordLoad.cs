using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Collections;
using System.IO;
using System.Xml;
using WordManager;



using ABB.Logger;



namespace XMLManager
{
    public class WordLoad
    {
        #region Declaration of variables
        bool objStatus;
        Word._Application iWdApp;
        Word._Document iDoc;
        Word.Table iTable;
        Word.Range iRange;
        public LoggerClass objLog = LoggerClass.getInstance();
        System.Guid guid;

        object missing = System.Reflection.Missing.Value;
        object oMissing = System.Reflection.Missing.Value;
        object Docfile;
        object readOnly = false;
        object isVisible = true;

        public string wdBookMark;

        #endregion
        #region Declaration of Constants
        private const int COLCOUNT = 4;
        #endregion

        #region Check status of word document loaded or not
        public bool isWordAppLoaded()
        {
            try
            {

                if (iDoc != null)
                    objStatus = true;
                else
                    objStatus = false;
            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString());
            }
            return this.objStatus;
        }
        #endregion

        public void Load()
        {

            string currentPath = Path.GetDirectoryName(System.Reflection.Assembly.
                 GetExecutingAssembly().GetName().CodeBase);
            Docfile = currentPath + "\\" + "Input\\Offer_Letter_Template.doc";
			//Docfile = "‪C:\\Offer_Letter_Template.doc";

            iWdApp = new Word.Application();



            try
            {

                iDoc = iWdApp.Documents.Open(ref Docfile, ref missing, ref readOnly,
                              ref missing, ref missing, ref missing,
                              ref missing, ref missing, ref missing,
                              ref missing, ref missing, ref isVisible,
                              ref missing, ref missing, ref missing,ref missing);

            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString() );
            }


        }



        public void UnLoad()
        {

            string currentPath = Path.GetDirectoryName(System.Reflection.Assembly.
                GetExecutingAssembly().GetName().CodeBase);
           object outputfile = currentPath + "\\" + "Output\\Project_Offer_Letter.doc";
		   
		   //object outputfile = "‪C:\\Project_Offer_Letter.doc";

            object savechange = false;


            try
            {
                if (iDoc != null)
                {
                    System.Diagnostics.EventLog.WriteEntry("Close", "");
                    iDoc.SaveAs(ref outputfile, ref missing, ref readOnly,
                               ref missing, ref missing, ref missing,
                               ref missing, ref missing, ref missing,
                               ref missing, ref missing, ref isVisible,
                               ref missing, ref missing, ref missing, ref missing);
                    iDoc.Close(ref savechange, ref missing, ref missing);
                    iWdApp.Documents.Close(ref savechange, ref missing, ref missing);
                


                    //iWdApp.Documents.Close(ref savechange, ref missing, ref missing);

                    //((Word._Document)iWdApp.Documents).Close(ref savechange, ref missing, ref missing);
                    //((Word._Application)iWdApp.Application).Quit(ref missing, ref missing, ref missing);


                }



            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString() );
            }
            finally
            {
                iDoc = null;
                iWdApp = null;
            }

        }

        public bool ISBookMarkAvailable(OfferLetterNode bookMarkNode)
        {

            try
            {
                String strSearchText = bookMarkNode.getPropertyValue("SearchText");


                for (int j = 0; j < iDoc.Bookmarks.Count; j++)
                {
                    if (iDoc.Bookmarks.Exists(strSearchText))
                    {
                        return true;
                    }

                }


            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString() );
            }
            return false;



        }

        public void CreateTechnicalDetailsTable(OfferLetterNode bookMarkNode)
        {

            TechnicalDetailsClass techiNodes = new TechnicalDetailsClass(bookMarkNode, iDoc);
        }


        public void CreateScopeDetailsTable(OfferLetterNode bookMarkNode)
        {


            ScopeDetailsClass techiNodes = new ScopeDetailsClass(bookMarkNode, iDoc);
        }

        public void CreateEquipmentDetailsTable(OfferLetterNode bookMarkNode)
        {


           EquipmentDetailsClass techiNodes = new EquipmentDetailsClass(bookMarkNode, iDoc);
        }

		public void CreateEquipmentDetailsTable1(OfferLetterNode bookMarkNode)
        {


           EquipmentDetailsClass1 techiNodes = new EquipmentDetailsClass1(bookMarkNode, iDoc);
        }


        public void  CreateSummaryTable(OfferLetterNode bookMarkNode)
        {
            SummaryTable techiNodes = new SummaryTable(bookMarkNode, iDoc);

        }


    }

}