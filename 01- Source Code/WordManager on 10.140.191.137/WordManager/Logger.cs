using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ABB.Logger
{
   public  class LoggerClass
    {

       private static LoggerClass logInstance;

        #region Declaration of Constants
        private const string FORMAT_HEADER = "Log Details :";
       System.Guid guid;
        #endregion

       private LoggerClass()
       {
           if (guid == Guid.Empty)
           {
               guid = Guid.NewGuid();


           }
       }

       public static LoggerClass getInstance()
       {
           if (logInstance == null)
           {
               logInstance = new LoggerClass();
           }

           return logInstance;
       }

       
        #region Write the message string into the Log file
        /// <summary>
        ///  Write the string that is passed into the log file 
        ///     /// </summary>
        /// <param name="message"></param>
        /// <param name="guid"></param>
        /// <param name="role"></param>

        public void   WriteLogFile(string message)
        {
            StreamWriter writer;
            
            string currentPath = Path.GetDirectoryName(System.Reflection.Assembly.
              GetExecutingAssembly().GetName().CodeBase);


            string strFilePath = currentPath + "/" + "LOGS/"+guid+".txt";
            strFilePath = strFilePath.Remove(0, 6);
            try
            {
                             

                //Create the file if the file is not existing
                if (!File.Exists(strFilePath))
                {

                    writer = File.CreateText(strFilePath);
                    writer.WriteLine(FORMAT_HEADER);
                    writer.Close();


                }
                //If the file exists, append the text in the file
                writer = File.AppendText(strFilePath);

                  //If the message is not formatting line
                    if (!message.Contains("-------------"))
                    {
                        writer.WriteLine(message);
                        writer.WriteLine();
                        writer.WriteLine("-------------------------------------------------------------------------"); 
                    }
                    else
                    {
                        writer.WriteLine(message);
                        
                    }
               
                writer.Close();
               
            }
            catch (Exception writingErrorException)
            {
                System.Diagnostics.EventLog.WriteEntry("WriteLogFileException", writingErrorException.Message.ToString());
            }
        
         
        }


        #endregion
       
       
    }
}
