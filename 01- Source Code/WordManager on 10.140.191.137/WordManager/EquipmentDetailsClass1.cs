﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using ABB.Logger;

namespace WordManager
{
    class EquipmentDetailsClass1
    {
        OfferLetterNode equipMentDetailsNode = null;
        Word._Document iDoc;
        Word.Table iTable;
        Word.Range iRange;
        Word.Paragraph iPara;

        object oMissing = System.Reflection.Missing.Value;
        public LoggerClass objlog = null;

        public EquipmentDetailsClass1(OfferLetterNode bookMarkNode, Word._Document iDoc)
        {

            objlog = LoggerClass.getInstance();

            try
            {
                this.equipMentDetailsNode = bookMarkNode;
                this.iDoc = iDoc;


                String strSearchText = equipMentDetailsNode.getPropertyValue("SearchText");
                object bMark = (object)strSearchText;
                iRange = iDoc.Bookmarks.get_Item(ref bMark).Range;
                object oRang = iRange;

                iPara = iDoc.Content.Paragraphs.Add(ref oRang);




                writeContents();
                iTable.Rows.SetHeight(12.0f, Word.WdRowHeightRule.wdRowHeightAtLeast);
            }
            catch (Exception objEx)
            {
                objlog.WriteLogFile(objEx.Message.ToString());
            }

        }

        public void writeContents()
        {
            try
            {


                List<OfferLetterNode> liEqipmentDeatils = equipMentDetailsNode.getChildrenOfType("Equipment1");

                for (int i = 0; i < liEqipmentDeatils.Count; i++)
                {
                    OfferLetterNode equipMentNode = liEqipmentDeatils[i];

                    writeEquipmentContents(equipMentNode, iPara.Range, i + 1);


                }
            }

            catch (Exception ObjWrExe)
            {
                objlog.WriteLogFile(ObjWrExe.Message.ToString());
            }

        }



        public void writeEquipmentContents(OfferLetterNode equipMentNode, Word.Range inRange, int count)
        {

            try
            {

                int finalCount = 0;

                List<OfferLetterNode> liEquipment = equipMentNode.getChildrenOfType("Part");

                //getTechnicalDetailsTableTotalRowCount
                int equipmentCount = liEquipment.Count;

                finalCount = equipmentCount + 2;

                creatTable(finalCount - 1, 3, inRange);
                iTable.Rows.Height = 15f;
                iTable.Cell(1, 1).Range.Text = Convert.ToString(count);
                iTable.Columns[1].Width = 25f;
                iTable.Rows[1].Cells.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray25;

                iTable.Rows[1].Range.Font.Name = "Arial";
                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;

                iTable.Cell(1, 2).Range.Text = equipMentNode.getPropertyValue("Name");
                iTable.Columns[2].Width = 400f;
                iTable.Cell(1, 3).Range.Text = "Quantity";
                iTable.Columns[3].Width = 75f;
                iTable.Rows[1].Range.Font.Name = "Arial";

                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;


                int rowcount = 2;

                for (int k = 0; k < liEquipment.Count; k++)
                {


                    OfferLetterNode equipNode = liEquipment[k];
                    String strEquipName = equipNode.getPropertyValue("Name");

                    String strEquipQty = equipNode.getPropertyValue("Quantity");


                    iTable.Cell(rowcount, 2).Range.Text = strEquipName;
                    iTable.Rows[rowcount].Range.Font.Name = "Arial";
                    iTable.Rows[rowcount].Range.Font.Bold = 0;
                    iTable.Rows[rowcount].Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;

                    iTable.Cell(rowcount, 3).Range.Text = strEquipQty;
                    iTable.Rows[rowcount].Range.Font.Name = "Arial";
                    iTable.Rows[rowcount].Range.Font.Bold = 0;
                    iTable.Rows[rowcount].Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;

                    rowcount++;
                }

                object orng = iPara.Range;
                Word.Paragraph iParatemp = iDoc.Paragraphs.Add(ref orng);
                iParatemp.Range.Text = "\r\r";
                // iPara = iPara.Next(ref countNo);
                object ct = 1;
                iPara = iPara.Previous(ref ct);

            }
            catch (Exception objEXE)
            {
                objlog.WriteLogFile(objEXE.Message.ToString());
            }


        }




        public void creatTable(int rows, int columns, Word.Range inRange)
        {
            try
            {

                iTable = iDoc.Content.Tables.Add(inRange, rows, columns, ref oMissing, ref oMissing);
                iTable.AllowPageBreaks = true;
                iTable.Borders.Enable = 1;
                iTable.Range.ParagraphFormat.SpaceAfter = 6;
                iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitWindow);

                // iTable.AllowAutoFit = true;
                for (int j = 0; j <= columns; j++)
                {
                    //if (j == 1 || j == 3)
                       // iTable.Columns[j].Width = 15.0f;

                }
            }
            catch (Exception objTabEXE)
            {
                objlog.WriteLogFile(objTabEXE.Message.ToString());
            }

        }

    }
}
