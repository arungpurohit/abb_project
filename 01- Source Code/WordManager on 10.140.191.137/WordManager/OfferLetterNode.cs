using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml;



using ABB.Logger;

namespace WordManager
{

    public class OfferLetterNode
    {
        #region Declaration of Variables
        public Hashtable objProp = null;
        public List<OfferLetterNode> objChildList = null;
        private OfferLetterNode parent;
        private XmlElement objElement;
        private String strTagName;
        public LoggerClass objLog = null;
        public Guid guid;
        #endregion
        #region Constructor
        public OfferLetterNode(XmlElement objElement)
        {

            objLog = LoggerClass.getInstance();
            if (objElement == null)
            {
                objLog.WriteLogFile("Element :"+objElement.Name+ "is null in xml document");
                throw new NullReferenceException("XML Element cannot be null");
            }
            objLog.WriteLogFile("Element found:"+objElement.Name+" xml document" );
            this.objElement = objElement;
            strTagName = objElement.Name;
            initFromNode();

        }
        #endregion
        #region Get Property and Children
        private void initFromNode()
        {
            try
            {
                
                XmlNodeList list = objElement.ChildNodes;

                objProp = new Hashtable();

                objChildList = new List<OfferLetterNode>();

                for (int i = 0; i < list.Count; i++)
                {
                    XmlNode node = list.Item(i);

                    if (node.ChildNodes.Count == 1)
                    {
                        objProp.Add(node.Name, node.InnerText);
                    }
                    else
                    {

                        OfferLetterNode childNode = new OfferLetterNode((XmlElement)node);
                        childNode.setParent(this);
                        objChildList.Add(childNode);
                    }
                }
            }
            catch (Exception inExe)
            {
                objLog.WriteLogFile(inExe.Message.ToString() );
            }
        }
        #endregion

        #region GetChildren
        public List<OfferLetterNode> getChildren()
        {
            return objChildList;
        }
        #endregion
        #region Get Parent
        public OfferLetterNode getParent()
        {
            return parent;
        }
        #endregion
        #region Setparent
        public void setParent(OfferLetterNode node)
        {
            this.parent = node;
        }
        #endregion
        #region Get Properties
        public Hashtable getProperties()
        {
            return this.objProp;
        }
        #endregion

        #region Get Properties
        public string getPropertyValue(String propertyName)
        {
            String value = "";
            if (objProp.ContainsKey(propertyName))
            {
                value = objProp[propertyName].ToString();
            }

            return value;
        }
        #endregion

        public String getName()
        {
            return this.strTagName;
        }

        public List<OfferLetterNode> getChildrenOfType(String strTag)
        {
            List<OfferLetterNode> resultList = new List<OfferLetterNode>();

            for (int i = 0; i < objChildList.Count; i++)
            {
                OfferLetterNode childNode = objChildList[i];

                if (childNode.getName() == strTag)
                {
                    resultList.Add(childNode);
                }

            }
            return resultList;
        }
    }
}
