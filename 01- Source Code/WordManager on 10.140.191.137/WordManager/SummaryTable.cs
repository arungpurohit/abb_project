﻿using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using ABB.Logger;

namespace WordManager
{
    class SummaryTable
    {

        OfferLetterNode summaryDetails = null;
        Word._Document iDoc;
        Word.Table iTable;
        Word.Range iRange;
        Word.Paragraph iPara;
        public LoggerClass objLog = null;
        object oMissing = System.Reflection.Missing.Value;

        int SummaryCount;

        public SummaryTable(OfferLetterNode bookMarkNode, Word._Document iDoc)
        {
            objLog = LoggerClass.getInstance();
             this.summaryDetails = bookMarkNode;
             this.iDoc = iDoc;

             writeContents();

            Console.WriteLine("SummaryTable INSIDE CONSTRUCTOR");

        }

        public void writeContents()
        {
            try
            {
                String strSearchText = summaryDetails.getPropertyValue("SearchText");

                Console.WriteLine("strSearchText");
                Console.WriteLine(strSearchText);

                object bMark = (object)strSearchText;
                iRange = iDoc.Bookmarks.get_Item(ref bMark).Range;
                object oRang = iRange;

                iPara = iDoc.Content.Paragraphs.Add(ref oRang);

                List<OfferLetterNode> liSwgear = summaryDetails.getChildrenOfType("NRDOL");

                SummaryCount = liSwgear.Count;
                Console.WriteLine("NRDOL COUNT(writeContents)");
                Console.WriteLine(SummaryCount);

                for (int i = liSwgear.Count; i > 0; i--)
                {
                    OfferLetterNode swGearNode = liSwgear[i - 1];

                    writeNRDOLContents(swGearNode);
                    iPara = iDoc.Content.Paragraphs.Add(ref oRang);

                    object orng = iPara.Range;
                    Word.Paragraph iParatemp = iDoc.Paragraphs.Add(ref orng);
                    iParatemp.Range.Text = "\r";
                    object ct = 1;
                    iPara = iPara.Previous(ref ct);

                    SummaryCount--;
                }
            }
            catch (Exception objEx)
            {
                objLog.WriteLogFile(objEx.Message.ToString());
            }

        }


        private void writeNRDOLContents(OfferLetterNode swGearNode)
        {
            try
            {
                writeSet1Table(swGearNode);

            }
            catch (Exception objswEX)
            {
                throw;
            }


        }

        private void writeSet1Table(OfferLetterNode swCubsNode)
        {

            Type2Coordination(swCubsNode);
            Accessories(swCubsNode);

        }

        private void Accessories(OfferLetterNode swCubsNode)
        {

            Console.WriteLine(" Accessories LOOPpppppppppppppp");
            String strAccessoriesName = swCubsNode.getPropertyValue("NRDOL");

            iPara.Range.Font.Name = "Arial";
            iPara.LeftIndent = 10;
            iPara.Range.Font.Bold = 1;

            iPara.Range.Text = "\r\r:NRDOL group2\r\r\r";

            Word.Range inRange = iPara.Range;


            try
            {
                int finalCount = 1;

                List<OfferLetterNode> liSwCubs = swCubsNode.getChildrenOfType("Set_1");

                int cubCount = liSwCubs.Count;
                Console.WriteLine("getAccessoryTableTotalRowCount ");
                Console.WriteLine(cubCount);

 
                for (int j = 0; j < cubCount; j++)
                {
                    List<OfferLetterNode> liModules = liSwCubs[j].getChildrenOfType("Accessories");
                    List<OfferLetterNode> liModules1 = liModules[j].getChildrenOfType("Accessory");

                    int modCount = liModules1.Count;
                    finalCount = finalCount + modCount;

                    Console.WriteLine("final count ");
                    Console.WriteLine(finalCount);
                }

                creatTable(finalCount, 2, inRange);

                iTable.Cell(1, 1).Range.Text = "Qty";
                iTable.Rows[1].Cells.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray25;

                iTable.Rows[1].Range.Font.Name = "Arial";
                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;

                iTable.Cell(1, 2).Range.Text = "Type";

                iTable.Rows[1].Range.Font.Name = "Arial";

                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;
                iTable.Cell(1, 1).WordWrap = false;

                int rowcount = 2;


                for (int j = 0; j < 1; j++)
                {
                    List<OfferLetterNode> liSwCubs1 = liSwCubs[j].getChildrenOfType("Accessories");
                    List<OfferLetterNode> liModules1 = liSwCubs1[j].getChildrenOfType("Accessory");

                   iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);

                    Console.WriteLine(" 1ST LOOPpppppppppppppp");

                    Console.WriteLine("Accessory  Count ");
                    Console.WriteLine(liSwCubs1.Count);


                    Console.WriteLine("Accessories  Count ");
                    Console.WriteLine(liModules1.Count);

                    for (int k = 0; k < liModules1.Count; k++)
                    {

                        // List<OfferLetterNode> liModulesNo = liModules[k].getChildrenOfType("Typical");
                        OfferLetterNode cubicleNode = liModules1[k];

                        String strCubName = cubicleNode.getPropertyValue("Qty");
                        String strCubQtyValue = cubicleNode.getPropertyValue("Type");


                        Console.WriteLine(" 2ND LOOP accesssssssssss");
                        Console.WriteLine(strCubName);

                        iTable.Cell(rowcount, 1).Range.Text = strCubName;

                        iTable.Rows[rowcount].Range.Font.Name = "Arial";
                        iTable.Rows[rowcount].Range.Font.Bold = 1;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;

                        iTable.Cell(rowcount, 2).Range.Text = strCubQtyValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 1;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;

                        iTable.Rows[rowcount].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                        rowcount++;
                    }

                    rowcount++;

                }

            }
            catch (Exception tableEx)
            {
                objLog.WriteLogFile(tableEx.Message.ToString());
            }
        }

        private void Type2Coordination(OfferLetterNode swCubsNode)
        {
            try
            {
                String strSwGearName = swCubsNode.getPropertyValue("NRDOL");
                Console.WriteLine("writeSwGearContents ");
                Console.WriteLine(strSwGearName);

                iPara.Range.Font.Name = "Arial";
                iPara.LeftIndent = 150;
                iPara.Range.Font.Bold = 1;
                iPara.Range.Text = "\r\r\r" + ": " + "NRDOL group1" + "\r\r";


                Word.Range inRange = iPara.Range;


                int finalCount = 1;

                List<OfferLetterNode> liSwCubs = swCubsNode.getChildrenOfType("Set_1");
                // List<OfferLetterNode> liSwCubs1 = liSwCubs[0].getChildrenOfType("Type2Coordination");

                //getcubiclemoduledetailsTableTotalRowCount
                int cubCount = liSwCubs.Count;
                Console.WriteLine("getsummaryTableTotalRowCount ");
                Console.WriteLine(cubCount);

               // finalCount = finalCount + (cubCount * 2) + 1;


                for (int j = 0; j < cubCount; j++)
                {
                    List<OfferLetterNode> liModules = liSwCubs[j].getChildrenOfType("Type2Coordination");
                    List<OfferLetterNode> liModules1 = liModules[j].getChildrenOfType("Typical");
                    int modCount = liModules1.Count;
                    finalCount = finalCount + modCount;

                    Console.WriteLine("final count ");
                    Console.WriteLine(finalCount);
                }

                creatTable(finalCount, 3, inRange);

                iTable.Cell(1, 1).Range.Text = "Rating [kW]";
                iTable.Rows[1].Cells.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray25;

                iTable.Rows[1].Range.Font.Name = "Arial";
                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;

                iTable.Cell(1, 2).Range.Text = "Coordination";

                iTable.Cell(1, 3).Range.Text = "Drawer size";

                iTable.Rows[1].Range.Font.Name = "Arial";

                iTable.Rows[1].Range.Font.Bold = 1;
                iTable.Rows[1].Range.Font.Underline = 0;
                iTable.Cell(1, 1).WordWrap = false;

                int rowcount = 2;


                for (int j = 0; j < cubCount; j++)
                {

                    List<OfferLetterNode> liModules = liSwCubs[j].getChildrenOfType("Type2Coordination");

                    List<OfferLetterNode> liModules1 = liModules[j].getChildrenOfType("Typical");

                    iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);

                    Console.WriteLine(" 1ST LOOPpppppppppppppp");

                    Console.WriteLine("Type2Coordination  Count ");
                    Console.WriteLine(liModules.Count);


                    Console.WriteLine("Typical  Count ");
                    Console.WriteLine(liModules1.Count);

                    for (int k = 0; k < liModules1.Count; k++)
                    {

                        // List<OfferLetterNode> liModulesNo = liModules[k].getChildrenOfType("Typical");
                        OfferLetterNode cubicleNode = liModules1[k];

                        String strCubName = cubicleNode.getPropertyValue("Rating_kW");
                        String strCubQtyValue = cubicleNode.getPropertyValue("Coordination");
                        String strCubQtyValue1 = cubicleNode.getPropertyValue("DrawerSize");


                        Console.WriteLine(" 1ST LOOP Rating_kW");
                        Console.WriteLine(strCubName);

                        iTable.Cell(rowcount, 1).Range.Text = strCubName;

                        iTable.Rows[rowcount].Range.Font.Name = "Arial";
                        iTable.Rows[rowcount].Range.Font.Bold = 1;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;

                        iTable.Cell(rowcount, 2).Range.Text = strCubQtyValue;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 1;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;

                        iTable.Cell(rowcount, 3).Range.Text = strCubQtyValue1;
                        iTable.Rows[rowcount].Range.Font.Name = "Arial";

                        iTable.Rows[rowcount].Range.Font.Bold = 1;
                        iTable.Rows[rowcount].Range.Font.Underline = 0;
                        iTable.Cell(rowcount, 1).WordWrap = false;


                        iTable.Rows[rowcount].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                        rowcount++;
                    }

                    rowcount++;

                }

            }
            catch (Exception tableEx)
            {

                Console.WriteLine("INSIDE CATCHHHHHHHH");
                objLog.WriteLogFile(tableEx.Message.ToString());
            }
        }

        public void creatTable(int rows, int columns, Word.Range inRange)
        {
            try
            {
                iTable = iDoc.Tables.Add(inRange, rows, columns, ref oMissing, ref oMissing);

                for (int j = 1; j < columns; j++)
                {
                    // if (j == 1 || j == 3)
                    //   iTable.Columns[j].Width = 0.0f;

                }
                iTable.AllowPageBreaks = true;
                iTable.Borders.Enable = 1;
                iTable.Range.ParagraphFormat.SpaceAfter = 6;
                iTable.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);



            }
            catch (Exception tablecrEX)
            {
                objLog.WriteLogFile("Scope Details Table creation Error");
            }
        }



    }
}
